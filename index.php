<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
global $DB,$USER;
if($_GET['mode'] == 'add'){
$title = "Добавить мероприятие";
}else if($_GET['mode'] == 'edit'){
$title = "Внести изменения: " . $_GET['name'];
}else if($_GET['mode'] == 'mail'){
$title = "Отправить запрос";
}else{
$title='Актовый зал';
}
$APPLICATION->SetTitle($title);
$DB->query('create table if not exists auditorium (id int primary key auto_increment,date date,start varchar(255),end varchar(255),name text,respon text,from_name text,uid int,email varchar(255));');
$result = $DB->query("select count(distinct date) as aeCount from auditorium where date >= curdate();");
$row = $result->fetch();
$admins=[
841,845
];
$uid = $USER->getId();
foreach($admins as $admin){
if($uid==$admin){
$aid=$uid;}
}
$email = $USER->getEmail();
$from_name = $USER->GetFullName();
$err=false;
$num=5;
$page=$_GET['page'];
$posts=$row['aeCount'];
$total=intval(($posts-1)/$num)+1;
$page=intval($page);
if(empty($page) or $page<0)$page=1;
if($page>$total) $page = $total;
$start=$page*$num-$num;
$DB->query('delete from auditorium where date < curdate();');
$dates = $DB->query('select distinct date from auditorium order by date asc limit '.$start.','.$num.';');
?>
<script
  src="https://code.jquery.com/jquery-3.5.1.js"
  integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
  crossorigin="anonymous"></script>
<script>
function sleep(f) {
  if(f=='window'){
    setTimeout(function() { window.open('/auditorium/?page=<?=$page?>','_self')}, 1500);
  }else{
    setTimeout(function() { history.back() }, 1500);
  }
}
    $(document).ready(function () {

        $('a#link1').click(function (e) {

                $("input[name='date_end']").val('');

                $('a#link1').text(function(i, text){
                        return text === "Задать период" ? "Отменить" : "Задать период";
                });

                $('div#qw').text(function(i, text){
                        return text === "Дата:" ? "Начало периода:" : "Дата:";
                });

                $('#content1').slideToggle();


        });

    });
</script>
<style>
#workarea-content{
font-size: 20px;
background: #f69a73;
}
blockquote {
border-radius: 20px;
margin: 0;
background: white;
border-top: 5px solid #EAF9F9;
border-bottom: 5px solid #EAF9F9;
color: #3A3C55;
padding: 30px 30px 30px 90px;
position: relative;
font-family: 'Lato', sans-serif;
font-weight: 300;
}
blockquote:before {
content: "!";
font-family: serif;
position: absolute;
left: 20px;
top: 20px;
color: white;
background: #FB6652;
width: 50px;
height: 50px;
border-radius: 50%;
font-size: 50px;
line-height: 1.00;
text-align: center;
}
blockquote p {
margin: 0 0 16px;
font-size: 22px;
letter-spacing: .05em;
line-height: 1.4;
}
blockquote cite {
font-style: normal;
}
#workarea-content *{
box-sizing: border-box;
}
.decor {
position: relative;
max-width: 400px;
margin: 50px auto 0;
background: white;
border-radius: 30px;
}
.form-left-decoration,
.form-right-decoration {
content: "";
position: absolute;
width: 50px;
height: 20px;
background: #f69a73;
border-radius: 20px;
}
.form-left-decoration {
bottom: 60px;
left: -30px;
}
.form-right-decoration {
top: 60px;
right: -30px;
}
.form-left-decoration:before,
.form-left-decoration:after,
.form-right-decoration:before,
.form-right-decoration:after {
content: "";
position: absolute;
width: 50px;
height: 20px;
border-radius: 30px;
background: white;
}
.form-left-decoration:before {
top: -20px;
}
.form-left-decoration:after {
top: 20px;
left: 10px;
}
.form-right-decoration:before {
top: -20px;
right: 0;
}
.form-right-decoration:after {
top: 20px;
right: 10px;
}
.circle {
position: absolute;
bottom: 80px;
left: -55px;
width: 20px;
height: 20px;
border-radius: 50%;
background: white;
}
.form-inner {
padding: 50px;
}
.form-inner input,
.form-inner textarea {
display: block;
width: 100%;
padding: 0 20px;
margin-bottom: 10px;
background: #E9EFF6;
line-height: 40px;
border-width: 0;
border-radius: 20px;
font-family: 'Roboto', sans-serif;
}
.form-inner input[type="submit"] {
cursor:pointer;
margin-top: 30px;
background: #f69a73;
border-bottom: 4px solid #d87d56;
color: white; font-size: 20px;
}
.form-inner input[type="submit"]:hover {
background: #E9EFF6;
color: #B05151; font-size: 25px;
}
.form-inner input[name="new_date"] {
width:60%;
display:inline-block;
}
.form-inner input[type="time"] {
width:49%;
display:inline-block;
}
.form-inner textarea {
resize: none;
}
.form-inner h3 {
margin-top: 0;
font-family: 'Roboto', sans-serif;
font-weight: 500;
font-size: 24px;
color: #707981;
}
table.adtrm_tbl {
width: 100%;
font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
text-align: center;
border-collapse: collapse;
border-spacing: 5px;
background: #E1E3E0;
border-radius: 20px;
} th.adtrm_th {
font-size: 22px;
font-weight: 300;
padding: 12px 10px;
border-bottom: 2px solid #F56433;
color: #F56433;
} tbody tr:nth-child(2) {
border-bottom: 2px solid #F56433;
} td.adtrm_td {
padding: 10px;
color: #8D8173;
}
div.qwe{
width:40%;
display:inline-block;
font-size: 15px;
color: #8D8173;
}
a.adtrm_lnk {
border-radius:10px;
background: #E1E3E0;
color: #B05151;
padding: 4px 4px;
position: relative;
box-shadow: 2px 2px 3px rgba(0,0,0,0.3);
}
a.adtrm_lnk:hover {
background: #F58262;
color: #ffe;
}
</style>
<br>
<?
$mode = $_GET['mode'];
switch($mode) {
default:
if($uid==$aid){
?>
<a class="adtrm_lnk" href="/auditorium/?mode=cnfrm"> [ Очистить таблицу мероприятий ] </a>
<br><br><br>
<?
}
?>
<a class="adtrm_lnk" href="/auditorium/?page=<?=$page?>&mode=add">Добавить мероприятие</a>
<br><br><br>
<? if($row['aeCount'] > 0) { ?>
<table class="adtrm_tbl">
<?
setlocale(LC_ALL, "ru_RU.UTF-8");
$month=['Январь' , 'Февраль' , 'Март' , 'Апрель' , 'Май' , 'Июнь' , 'Июль' , 'Август' , 'Сентябрь' , 'Октябрь' , 'Ноябрь' , 'Декабрь'];
$e_month=['Января' , 'Февраля' , 'Марта' , 'Апреля' , 'Мая' , 'Июня' , 'Июля' , 'Августа' , 'Сентября' , 'Октября' , 'Ноября' , 'Декабря'];
while($info0 = $dates->fetch()){
$data = $DB->query('select * from auditorium where date="'.$info0["date"].'" order by start;');
?>
<tr>
<th class="adtrm_th" colspan="5"><?=str_replace($month,$e_month,strftime("%A, %d %B %Y",strtotime($info0['date'])));?></th>
</tr> 
<tr>
<th class="adtrm_th">Начало</th>
<th class="adtrm_th">Конец</th>
<th class="adtrm_th">Мероприятие</th>
<th class="adtrm_th">Кто проводит</th>
<th class="adtrm_th">Дополнительно</th>
</tr>
<?
while($info = $data->fetch()){
?>
<tr>
<td class="adtrm_td"><?=$info['start'];?></td> <td class="adtrm_td"><?=$info['end'];?></td> <td class="adtrm_td"><?=urldecode($info['name']);?></td> <td class="adtrm_td"><?=$uid==$aid?urldecode($info['respon']).'<br><hr><a class="adtrm_lnk" href="/company/personal/user/'.$info['uid'].'/">'.$info["from_name"].'</a>':urldecode($info['respon']);?></td>
<td class="adtrm_td">
<?
if($uid==$info['uid'] or $uid==$aid){
?>
<a class="adtrm_lnk" href="/auditorium/?page=<?=$page?>&id=<?=$info['id'];?>&date=<?=$info['date'];?>&start=<?=$info['start'];?>&end=<?=$info['end'];?>&name=<?=$info['name'];?>&respon=<?=$info['respon'];?>&mode=edit">Внести изменения</a>
  или  
<a class="adtrm_lnk" href="/auditorium/?page=<?=$page?>&id=<?=$info['id'];?>&mode=remove">отменить</a> мероприятие.
<?
}else{
?>
<a class="adtrm_lnk" href="/auditorium/?page=<?=$page?>&id=<?=$info['id'];?>&date=<?=$info['date'];?>&start=<?=$info['start'];?>&end=<?=$info['end'];?>&name=<?=$info['name'];?>&respon=<?=$info['respon'];?>&email=<?=$info['email'];?>&mode=mail">Попросить внести изменения или отменить мероприятие</a>
<?
}
?>
</td>
</tr>
<?
}
?>
<td class="adtrm_td" colspan="5"><br></td>
<?
}
?>
</table>
<?
if ($page>4)$pn_dots='...';
if ($page<$total-3)$pn__dots='...';
if ($page>3)$pervpage='<a  class="adtrm_lnk" href=?page=1>&nbsp1'.$pn_dots.'&nbsp</a>&nbsp';
if($page<$total-2)$nextpage='&nbsp<a  class="adtrm_lnk" href=?page='.$total.'>&nbsp'.$pn__dots.$total.'&nbsp</a>';
if($page-2>0)$page2left='<a  class="adtrm_lnk" href=?page='.($page-2).'>&nbsp'.($page-2).'&nbsp</a>&nbsp';
if($page-1>0)$page1left='<a  class="adtrm_lnk" href=?page='.($page-1).'>&nbsp'.($page-1).'&nbsp</a>&nbsp';
if($page+2<=$total)$page2right='&nbsp<a  class="adtrm_lnk" href=?page='.($page+2).'>&nbsp'.($page+2).'&nbsp</a>';
if($page+1<=$total)$page1right='&nbsp<a  class="adtrm_lnk" href=?page='.($page+1).'>&nbsp'.($page+1).'&nbsp</a>';
if($total>1){echo '<h3><p align="center">'.$pervpage.$page2left.$page1left.'&nbsp<b style="color:white">'.$page.'</b>&nbsp'.$page1right.$page2right.$nextpage.'</p></h3>';}
} else {
?>
<blockquote>
<p>Мероприятий не запланировано...</p>
<footer><cite>Нажмите "Добавить мероприятие", чтобы создать новое.</cite></footer>
</blockquote>
<?
}
break;
case 'add':
?>
<form class="decor" action="/auditorium/?page=<?=$page?>&mode=added" method="post">
<div class="form-left-decoration"></div>
<div class="form-right-decoration"></div>
<div class="circle"></div> <div class="form-inner">
<h3>Добавить мероприятие</h3>
<div id= "qw"  class="qwe">Дата:</div>
<input name="new_date" type="date" placeholder="Дата" required><div class="qwe" style="text-align:center"><a class="adtrm_lnk" id="link1" style="cursor:pointer">Задать период</a></div>
<div id="content1" style="display: none;">
<div class="qwe">Конец периода:</div>
<input name="date_end" type="date" placeholder="Дата">
</div>
<div class="qwe">Начало:</div><div class="qwe" style="text-align:center">Конец:</div>
<input name="start" type="time" placeholder="Время" required>
<input name="end" type="time" placeholder="Время" required>
<div class="qwe">Мероприятие:</div>
<textarea name="name" placeholder="Название / описание / примечания..." rows="3" required></textarea>
<div class="qwe">Кто проводит:</div>
<textarea name="respon" placeholder="ФИО / отдел..." rows="3" required></textarea>
<input type="submit" value="Добавить">
<a class="adtrm_lnk" href='/auditorium/?page=<?=$page?>';>Вернуться</a>
</div>
</form>
<?
break;
case 'added':
$date = $_POST['new_date'];
$date_end = $_POST['date_end'];
$start = $_POST['start'];
$end = $_POST['end'];
$name = urlencode($_POST['name']);
$respon = urlencode($_POST['respon']);
$chk=$DB->query("select start,end from auditorium where date='".$date."';");
while($check = $chk->fetch()){
	if(($start>=$check['start'] and $start<$check['end']) or ($end>$check['start'] and $end<=$check['end']) or ($start<=$check['start'] and $end>=$check['end'])){
    $err=true;
  }
}
if ($start>=$end){
		$err=true;
}
if ($date<date('Y-m-d')){
		$err=true;
}
if ($date_end!=0000-00-00 and $date_end<=$date){
		$err=true;
}
if($err){
?>
<blockquote>
<p>Отказано!</p>
<footer><cite>Форма содержит ошибки.</cite></footer>
</blockquote>
<?
echo "<script>sleep('history');</script>";
}else{
do{
$DB->query("insert into auditorium (date, start, end, name, respon, from_name, uid, email) values ( '" . $date . "' , '" . $start . "','". $end . "','" . $name . "','". $respon . "','" . $from_name . "','" . $uid . "','". $email . "')");
$date=strftime("%Y-%m-%d", strtotime("$date +1 day")); 
}while($date<=$date_end);
?>
<blockquote>
<p>Выполнено!</p>
<footer><cite>Запись успешно создана.</cite></footer>
</blockquote>
<?
echo "<script>sleep('window');</script>";
}
break;
case 'edit':
$checker=$DB->query("select count(id) as eCount from auditorium where id=".$_GET['id'].";");
$rowchecker = $checker->fetch();
if($rowchecker['eCount']!=0) {
?>
<form class="decor" action="/auditorium/?page=<?=$page?>&mode=edited" method="post">
<div class="form-left-decoration"></div>
<div class="form-right-decoration"></div>
<div class="circle"></div> <div class="form-inner">
<h3>Внести изменения</h3>
<div class="qwe">Дата:</div>
<input name="date" type="date" placeholder="Дата" value="<?=$_GET['date'];?>" required>
<div class="qwe">Начало:</div><div class="qwe" style="text-align:center">Конец:</div>
<input name="start" type="time" placeholder="Время" value="<?=$_GET['start'];?>" required>
<input name="end" type="time" placeholder="Время" value="<?=$_GET['end'];?>" required>
<div class="qwe">Мероприятие:</div>
<textarea name="name" placeholder="Название / описание / примечания..." rows="3" required><?=$_GET['name'];?></textarea>
<div class="qwe">Кто проводит:</div>
<textarea name="respon" placeholder="ФИО / отдел..." rows="3" required><?=$_GET['respon'];?></textarea>
<input type="submit" value="Сохранить изменения">
<input type="hidden" name="id" value="<?=$_GET['id'];?>">
<a class="adtrm_lnk" href="/auditorium/?page=<?=$page?>">Вернуться</a>
</div>
</form>
<?
}else{
?>
<blockquote>
<p>Не найдено.</p>
<footer><cite>Запись была удалена</cite></footer>
</blockquote>
<?
echo "<script>sleep('window');</script>";
}
break;
case 'edited':
$id = $_POST['id'];
$date = $_POST['date'];
$start = $_POST['start'];
$end = $_POST['end'];
$name = urlencode($_POST['name']);
$respon = urlencode($_POST['respon']);
$chk=$DB->query("select start,end from auditorium where date='".$date."' and id!='".$id."';");
while($check = $chk->fetch()){
	if(($start>=$check['start'] and $start<$check['end']) or ($end>$check['start'] and $end<=$check['end']) or ($start<=$check['start'] and $end>=$check['end'])){
    $err=true;
  }
}
if ($start>=$end){
		$err=true;
}
if ($date<date('Y-m-d')){
		$err=true;
}
if($err){
?>
<blockquote>
<p>Отказано!</p>
<footer><cite>Форма содержит ошибки.</cite></footer>
</blockquote>
<?
echo "<script>sleep('history');</script>";
}else{
$DB->query("update auditorium set date = '" . $date . "' ,  start = '" . $start . "' , end = '" . $end . "' ,  name = '" . $name . "' , respon = '" . $respon . "' where id = '" . $id . "';");
?>
<blockquote>
<p>Выполнено!</p>
<footer><cite>Запись успешно изменена.</cite></footer>
</blockquote>
<?
echo "<script>sleep('window');</script>";
}
break;
case 'remove':
$id = $_GET['id'];
$DB->query("delete from auditorium where id= '" . $id ."';");
?>
<blockquote>
<p>Выполнено!</p>
<footer><cite>Запись успешно удалена.</cite></footer>
</blockquote>
<?
echo "<script>sleep('window');</script>";
break;
case 'mail':
?>
<form class="decor" action="/auditorium/?page=<?=$page?>&mode=sendmail" method="post">
<div class="form-left-decoration"></div>
<div class="form-right-decoration"></div>
<div class="circle"></div> <div class="form-inner">
<h3>Отправить запрос</h3>
<textarea name="msg" placeholder="Это сообщение увидит владелец записи" rows="3" required></textarea>
<input type="submit" value="Отправить">
<input type="hidden" name="name" value='<?=$_GET["name"];?>'>
<input type="hidden" name="email" value='<?=$_GET["email"];?>'>
<input type="hidden" name="date" value='<?=$_GET["date"];?>'>
<input type="hidden" name="start" value='<?=$_GET["start"];?>'>
<input type="hidden" name="end" value='<?=$_GET["end"];?>'>
<input type="hidden" name="rm_lnk" value='<a href="https://b24.vkmp.by/auditorium/?id=<?=$_GET["id"];?>&mode=remove">отменить</a>'>
<input type="hidden" name="edt_lnk" value='<a href="https://b24.vkmp.by/auditorium/?id=<?=$_GET["id"];?>&date=<?=$_GET["date"];?>&start=<?=$_GET["start"];?>&end=<?=$_GET["end"];?>&name=<?=urlencode($_GET["name"]);?>&respon=<?=urlencode($_GET["respon"]);?>&mode=edit">внести изменения</a>'>
<a class="adtrm_lnk" href='/auditorium/?page=<?=$page?>'>Вернуться</a>
</div>
</form>
<?
break;
case 'sendmail':
$edt_lnk = $_POST['edt_lnk'];
$rm_lnk = $_POST['rm_lnk'];
$name = $_POST['name'];
$date = date("d . m . Y",strtotime($_POST['date']));
$start = $_POST['start'];
$end = $_POST['end'];
$u_email = $_POST['email'];
$msg = $_POST['msg'].'<br><br><hr><br>Вы можете '.$edt_lnk.'<br>или<br>'.$rm_lnk.' мероприятие "'.$name.'",<br>запланированное на '.$date.'<br>с '.$start.' до '.$end.'.';
$headers  = "Content-type: text/html; charset=utf-8 \r\n";
$headers .= "From: ".$from_name." <".$email.">\r\n"; 
mail($u_email,'Запрос на внесение изменений или отмену мероприятия "'.$name.'"',$msg,$headers);
?>
<blockquote>
<p>Выполнено!</p>
<footer><cite>Запрос успешно отправлен.</cite></footer>
</blockquote>
<?
echo "<script>sleep('window');</script>";
break;
case 'cnfrm':
?>
<script>
var cnfrm=prompt('Пароль:','');
if(cnfrm=='clear'){
window.open('/auditorium/?mode=clear','_self');
}else{
window.open('/auditorium/','_self');
}
</script>
<?
break;
case 'clear':
$DB->query('drop table auditorium');
echo "<script>window.open('/auditorium/','_self');</script>";
break;
}
?>
<br><br>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
